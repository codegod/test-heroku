import { shallow } from '@vue/test-utils';
import { expect } from 'chai';
import SIMCMenu from '@/components/ListErrors';

const data = 'Eroare';
const wrapper = shallow(SIMCMenu, { propsData: { error: { data } } });

describe('SIMCMenu.vue', () => {
  it('should render correct contents', () => {
    const sp = wrapper.find('span');
    expect(sp.text()).to.equal(data);
  });
});
