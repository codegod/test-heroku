/* eslint-disable import/prefer-default-export */
/**
 * Returns number of days in specific month
 * @param {String} date string which represent month with format 'YYYY-MM'
 */
export const getDaysOfMonth = (date) => {
  const parts = date.split('-');

  return new Date(parts[0], parts[1], 0).getDate();
};
