import JwtService from '@/common/jwt.service';
import ApiService from '@/common/api.service';
import { GET_CURRENT_USER, IS_AUTHENTICATED } from './getters.type';
import { SET_AUTH, SET_ERROR, PURGE_AUTH } from './mutations.type';
import { LOGIN, LOGOUT, CHECK_AUTH } from './actions.type';

const state = {
  error: {},
  user: {},
  isAuthenticated: !!JwtService.getToken(),
};

const getters = {
  [GET_CURRENT_USER](gState) {
    return gState.user;
  },
  [IS_AUTHENTICATED](gState) {
    return gState.isAuthenticated;
  },
};

const actions = {
  [LOGIN](context, credentials) {
    return new Promise((resolve, reject) => {
      ApiService.post('users/login/', credentials)
        .then(({ data }) => {
          context.commit(SET_AUTH, data);
          resolve(data);
        })
        .catch(({ response }) => {
          reject();
          context.commit(SET_ERROR, response || { data: 'Network error' });
        });
    });
  },
  [LOGOUT](context) {
    context.commit(PURGE_AUTH);
  },
  [CHECK_AUTH](context) {
    const hasToken = !!JwtService.getToken();
    if (hasToken) {
      ApiService.setHeader();
      ApiService.get('users', 'me')
        .then(({ data }) => {
          context.commit(SET_AUTH, { user: data });
        })
        .catch(({ response }) => {
          if (response === undefined || response.status === 401) {
            context.commit(PURGE_AUTH);
          } else {
            context.commit(SET_ERROR, response.data);
          }
        });
    } else {
      context.commit(PURGE_AUTH);
    }
  },
};

/* eslint-disable no-param-reassign */
const mutations = {
  [SET_ERROR](mState, error) {
    mState.error = error;
  },
  [SET_AUTH](mState, data) {
    mState.isAuthenticated = true;
    mState.user = data.user;
    mState.error = {};
    const hasToken = !!data.token;
    if (hasToken) {
      JwtService.saveToken(data.token);
    }
  },
  [PURGE_AUTH](mState) {
    mState.isAuthenticated = false;
    mState.user = {};
    mState.error = {};
    JwtService.destroyToken();
  },
};
/* eslint-enable no-param-reassign */

export default {
  state,
  actions,
  mutations,
  getters,
};
