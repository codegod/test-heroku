import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import JwtService from './jwt.service';
import { API_URL } from './config';

export default class ApiService {
  static init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = API_URL;
  }

  static setHeader() {
    Vue.axios.defaults.headers.common.Authorization = JwtService.getToken();
  }

  static query(resource, params) {
    return Vue.axios.get(resource, { params: params || {} });
  }

  static get(resource, slug = '') {
    return Vue.axios.get(`${resource}/${slug ? `${slug}/` : ''}`);
  }

  static post(resource, params) {
    return Vue.axios.post(resource, params);
  }

  static patch(resource, slug, params) {
    return Vue.axios.patch(`${resource}/${slug}/`, params);
  }

  static put(resource, params) {
    return Vue.axios.put(resource, params);
  }

  static delete(resource) {
    return Vue.axios.delete(resource).catch((error) => {
      throw new Error(`ApiService ${error}`);
    });
  }
}
