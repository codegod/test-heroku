export const ID_TOKEN_KEY = 'id_token';
export const API_URL = process.env.API_URL || '//api.simc.cf/v1/';

export default {
  ID_TOKEN_KEY,
  API_URL,
};
