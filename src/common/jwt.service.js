import { ID_TOKEN_KEY } from './config';

export default class JwtService {
  static getToken() {
    return window.localStorage.getItem(ID_TOKEN_KEY);
  }

  static saveToken(token) {
    window.localStorage.setItem(ID_TOKEN_KEY, token);
  }

  static destroyToken() {
    window.localStorage.removeItem(ID_TOKEN_KEY);
  }
}
