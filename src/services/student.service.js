import ApiService from '@/common/api.service';

export default class StudentService extends ApiService {
    static RESOURCE_NAME = 'students';

    static query(params = {}) {
      const { RESOURCE_NAME } = StudentService;
      return ApiService.query(`${RESOURCE_NAME}/`, params);
    }

    static get(id) {
      return ApiService.get(StudentService.RESOURCE_NAME, id);
    }
}
