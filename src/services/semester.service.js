import ApiService from '@/common/api.service';

export default class SemesterService extends ApiService {
    static RESOURCE_NAME = 'semesters';

    static query(params = {}) {
      const { RESOURCE_NAME } = SemesterService;
      return ApiService.query(`${RESOURCE_NAME}/`, params);
    }

    static get(id) {
      return ApiService.get(SemesterService.RESOURCE_NAME, id);
    }
}
