import ApiService from '@/common/api.service';

export default class SubjectService extends ApiService {
    static RESOURCE_NAME = 'subjects';

    static query(params = {}) {
      const { RESOURCE_NAME } = SubjectService;
      return ApiService.query(`${RESOURCE_NAME}/`, params);
    }

    static get(id) {
      return ApiService.get(SubjectService.RESOURCE_NAME, id);
    }
}
