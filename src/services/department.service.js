import ApiService from '@/common/api.service';

export default class DepartmentService extends ApiService {
    static RESOURCE_NAME = 'departments';

    static query(params = {}) {
      const { RESOURCE_NAME } = DepartmentService;
      return ApiService.query(`${RESOURCE_NAME}/`, params);
    }

    static get(id) {
      return ApiService.get(DepartmentService.RESOURCE_NAME, id);
    }
}
