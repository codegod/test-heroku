import ApiService from '@/common/api.service';

export default class TimetablesService extends ApiService {
    static RESOURCE_NAME = 'timetables';

    static query(params = {}) {
      const { RESOURCE_NAME } = TimetablesService;
      return ApiService.query(`${RESOURCE_NAME}/`, params);
    }

    static get(id) {
      return ApiService.get(TimetablesService.RESOURCE_NAME, id);
    }
}
