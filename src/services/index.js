import AbsenceService from './absence.service';
import DepartmentService from './department.service';
import GroupService from './group.service';
import MarkService from './mark.service';
import SemesterService from './semester.service';
import SpecialityService from './speciality.service';
import StudentService from './student.service';
import SubjectService from './subject.service';
import TimetablesService from './timetables.service';

export {
  AbsenceService,
  DepartmentService,
  GroupService,
  MarkService,
  SemesterService,
  SpecialityService,
  StudentService,
  SubjectService,
  TimetablesService,
};
