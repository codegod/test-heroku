import ApiService from '@/common/api.service';

export default class SpecialityService extends ApiService {
    static RESOURCE_NAME = 'specialities';

    static query(params = {}) {
      const { RESOURCE_NAME } = SpecialityService;
      return ApiService.query(`${RESOURCE_NAME}/`, params);
    }

    static get(id) {
      return ApiService.get(SpecialityService.RESOURCE_NAME, id);
    }
}
