import ApiService from '@/common/api.service';

export default class AbsenceService extends ApiService {
    static RESOURCE_NAME = 'absences';

    static query(params = {}) {
      const { RESOURCE_NAME } = AbsenceService;
      return ApiService.query(`${RESOURCE_NAME}/`, params);
    }

    static get(id) {
      return ApiService.get(AbsenceService.RESOURCE_NAME, id);
    }
}
