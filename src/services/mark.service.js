import ApiService from '@/common/api.service';

/**
 * Makes requests on marks enpoints
 */
export default class MarkService extends ApiService {
  static RESOURCE_NAME = 'marks';

  static query(params = {}) {
    const { RESOURCE_NAME } = MarkService;
    return ApiService.query(`${RESOURCE_NAME}/`, params);
  }

  static get(id) {
    return ApiService.get(MarkService.RESOURCE_NAME, id);
  }
}
