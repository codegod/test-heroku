import ApiService from '@/common/api.service';

export default class GroupService extends ApiService {
  static RESOURCE_NAME = 'groups';

  static query(params = {}) {
    const { RESOURCE_NAME } = GroupService;
    return ApiService.query(`${RESOURCE_NAME}/`, params);
  }

  static get(id, slug = '', params = {}) {
    const { RESOURCE_NAME } = GroupService;
    return ApiService.query(`${RESOURCE_NAME}/${id}/${slug ? `${slug}/` : ''}`, params);
  }
}
