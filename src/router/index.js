import Vue from 'vue';
import Router from 'vue-router';
import store from '@/store';
import JwtService from '@/common/jwt.service';
import { CHECK_AUTH } from '@/store/actions.type';

Vue.use(Router);

function authRoute(path, view, addon) {
  return Object.assign(
    {
      path,
      name: view,
      meta: { requiresAuth: true },
      component: () => import(`@/views/${view}`),
    },
    addon || {},
  );
}

const router = new Router({
  mode: 'history',
  routes: [
    authRoute('/dashboard', 'Dashboard'),
    authRoute('/profile', 'Profile'),
    {
      path: '/departments',
      component: { template: '<router-view></router-view>' },
      children: [authRoute('', 'departments/list'), authRoute(':id', 'departments/department')],
    },
    {
      path: '/specialities',
      component: { template: '<router-view></router-view>' },
      children: [authRoute('', 'specialities/list'), authRoute(':id', 'specialities/speciality')],
    },
    {
      path: '/students',
      component: { template: '<router-view></router-view>' },
      children: [authRoute('', 'students/list'), authRoute(':id', 'students/student')],
    },
    {
      path: '/groups',
      component: { template: '<router-view></router-view>' },
      children: [
        authRoute('', 'groups/list'),
        authRoute(':id', 'groups/group'),
        authRoute(':id/register', 'groups/register'),
      ],
    },
    {
      path: '/marks',
      component: { template: '<router-view></router-view>' },
      children: [
        {
          path: '',
          meta: { requiresAuth: true },
          component: () => import('@/views/students/list'),
        },
        authRoute(':student', 'marks/marks'),
      ],
    },
    {
      path: '/absences',
      component: { template: '<router-view></router-view>' },
      children: [
        {
          path: '',
          meta: { requiresAuth: true },
          component: () => import('@/views/students/list'),
        },
        authRoute(':student', 'absences/absences'),
      ],
    },
    {
      path: '/timetables',
      component: { template: '<router-view></router-view>' },
      children: [
        { path: '', meta: { requiresAuth: true }, component: () => import('@/views/groups/list') },
        authRoute(':group', 'timetables/list'),
      ],
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import('@/views/Login'),
      meta: { onlyGuests: true },
    },
    { path: '*', redirect: '/dashboard' },
  ],
});

router.beforeEach((to, from, next) => {
  const hasToken = !!JwtService.getToken();
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!hasToken) {
      next({ path: '/login', query: { redirect: to.fullPath } });
    } else {
      Promise.all([store.dispatch(CHECK_AUTH)]).then(next);
    }
  } else if (to.matched.some(record => record.meta.onlyGuests) && hasToken) {
    next({ path: '/' });
  } else {
    Promise.all([store.dispatch(CHECK_AUTH)]).then(next);
  }
});

export default router;
