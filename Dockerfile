FROM node:9-alpine as builder

WORKDIR /simc-code

COPY package.json yarn.lock ./

RUN yarn install

COPY . .

RUN source config/env/env.sh && yarn build

FROM nginx:alpine

ENV SYSGROUP susers
ENV SYSUSER suser
ENV USER_HOME /home/${SYSUSER} 

RUN addgroup -S ${SYSGROUP} && \
  adduser -S -G ${SYSGROUP} ${SYSUSER} && \
  touch /var/run/nginx.pid && \
  mkdir ${USER_HOME}/volatile && \
  chown -R ${SYSUSER}:${SYSGROUP} /var/run/nginx.pid && \
  chown -R ${SYSUSER}:${SYSGROUP} /var/cache/nginx/ || : && \
  chown -R ${SYSUSER}:${SYSGROUP} /var/log/nginx && \
  chown -R ${SYSUSER}:${SYSGROUP} /etc/nginx/conf.d

WORKDIR ${USER_HOME}/simc-code

COPY --from=builder /simc-code/dist/ ./dist

COPY config/server/default.conf ${USER_HOME}/volatile/temp-nginx.conf
COPY config/server/nginx.conf /etc/nginx/nginx.conf

RUN chown -R ${SYSUSER}:${SYSGROUP} ${USER_HOME}

USER ${SYSUSER}

CMD PORT=${PORT:-8080} && \
    cat ${USER_HOME}/volatile/temp-nginx.conf | \
    sed -e 's/$PORT/'"$PORT"'/g' > /etc/nginx/conf.d/default.conf; \
    nginx
