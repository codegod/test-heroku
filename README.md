# simc-front-end

> Front-end part of SIMC project

[![pipeline status](https://gitlab.com/simc-ceef/front-end/badges/master/pipeline.svg)](https://gitlab.com/simc-ceef/front-end/commits/master)
[![coverage report](https://gitlab.com/simc-ceef/front-end/badges/master/coverage.svg?job=test)](https://gitlab.com/simc-ceef/front-end/commits/master)

## Build Setup

```bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
yarn dev

# build for production with minification
yarn build

# build for production and view the bundle analyzer report
yarn build --report

# run unit tests
yarn unit

# run e2e tests
yarn e2e

# run all tests
yarn test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
